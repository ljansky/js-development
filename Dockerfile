FROM ubuntu:16.04

RUN apt-get update && apt-get install -y nodejs npm curl mc

RUN npm install n pm2 webpack babel-cli -g

RUN n stable

COPY install-nm.sh /usr/local/bin/install-nm.sh
RUN chmod 777 /usr/local/bin/install-nm.sh

COPY link-nm.sh /usr/local/bin/link-nm.sh
RUN chmod 777 /usr/local/bin/link-nm.sh