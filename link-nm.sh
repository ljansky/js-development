#!/bin/bash
for d in `find . -maxdepth 1 -mindepth 1 -type d -printf '%f\n'`; do
	[ -h /usr/src/out/$d/node_modules ] && unlink /usr/src/out/$d/node_modules
	ln -s /usr/src/nm/$d/node_modules /usr/src/out/$d/node_modules
	if [ "$d" != "app" ] ; then 
		cd ./$d && npm link && cd .. ; 
	fi
done

for d in `find . -maxdepth 1 -mindepth 1 -type d -printf '%f\n'`; do
	if [ "$d" != "app" ] ; then 
		cd ./app && npm link $d && cd .. ;
	fi
done